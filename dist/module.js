import "./module.css";
import {jsx as $hgUW1$jsx} from "react/jsx-runtime";
import "react";

function $parcel$exportWildcard(dest, source) {
  Object.keys(source).forEach(function(key) {
    if (key === 'default' || key === '__esModule' || dest.hasOwnProperty(key)) {
      return;
    }

    Object.defineProperty(dest, key, {
      enumerable: true,
      get: function get() {
        return source[key];
      }
    });
  });

  return dest;
}
function $parcel$export(e, n, v, s) {
  Object.defineProperty(e, n, {get: v, set: s, enumerable: true, configurable: true});
}
function $parcel$interopDefault(a) {
  return a && a.__esModule ? a.default : a;
}
function $parcel$defineInteropFlag(a) {
  Object.defineProperty(a, '__esModule', {value: true, configurable: true});
}


var $a9efb3a368305fba$exports = {};



var $4c0580ffcf215c3b$exports = {};

$parcel$export($4c0580ffcf215c3b$exports, "Btn", () => $4c0580ffcf215c3b$export$37f54d0b7407611f);



const $4c0580ffcf215c3b$export$37f54d0b7407611f = ({ children: children , onClick: onClick  })=>{
    return /*#__PURE__*/ (0, $hgUW1$jsx)("button", {
        className: "Btn",
        onClick: onClick,
        children: children
    });
};


var $27a24320cac0429f$exports = {};

$parcel$export($27a24320cac0429f$exports, "Icon", () => $27a24320cac0429f$export$f04a61298a47a40f);



var $a1b4495e10b251c2$exports = {};

$parcel$defineInteropFlag($a1b4495e10b251c2$exports);

$parcel$export($a1b4495e10b251c2$exports, "default", () => $a1b4495e10b251c2$export$2e2bcd8739ae039);
var $04831d20f93d63ce$exports = {};
$04831d20f93d63ce$exports = new URL("help.cfb7a18f.png", import.meta.url).toString();


var $6374c7d582c99dc3$exports = {};
$6374c7d582c99dc3$exports = new URL("shelter.952f7ece.png", import.meta.url).toString();


const $a1b4495e10b251c2$var$IconsList = {
    help: (/*@__PURE__*/$parcel$interopDefault($04831d20f93d63ce$exports)),
    shelter: (/*@__PURE__*/$parcel$interopDefault($6374c7d582c99dc3$exports))
};
var $a1b4495e10b251c2$export$2e2bcd8739ae039 = (icon)=>{
    return $a1b4495e10b251c2$var$IconsList[icon];
};


const $27a24320cac0429f$export$f04a61298a47a40f = ({ i: i , alt: alt  })=>{
    return /*#__PURE__*/ (0, $hgUW1$jsx)("img", {
        className: "Icon",
        src: (0, $a1b4495e10b251c2$export$2e2bcd8739ae039)(i) + "",
        alt: alt || ""
    });
};



$parcel$exportWildcard($a9efb3a368305fba$exports, $4c0580ffcf215c3b$exports);
$parcel$exportWildcard($a9efb3a368305fba$exports, $27a24320cac0429f$exports);
$parcel$exportWildcard($a9efb3a368305fba$exports, $a1b4495e10b251c2$exports);




export {$4c0580ffcf215c3b$export$37f54d0b7407611f as Btn, $27a24320cac0429f$export$f04a61298a47a40f as Icon};
//# sourceMappingURL=module.js.map
