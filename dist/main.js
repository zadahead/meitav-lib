require("./module.css");
var $8zHUo$reactjsxruntime = require("react/jsx-runtime");
require("react");

function $parcel$exportWildcard(dest, source) {
  Object.keys(source).forEach(function(key) {
    if (key === 'default' || key === '__esModule' || dest.hasOwnProperty(key)) {
      return;
    }

    Object.defineProperty(dest, key, {
      enumerable: true,
      get: function get() {
        return source[key];
      }
    });
  });

  return dest;
}
function $parcel$export(e, n, v, s) {
  Object.defineProperty(e, n, {get: v, set: s, enumerable: true, configurable: true});
}
function $parcel$interopDefault(a) {
  return a && a.__esModule ? a.default : a;
}
function $parcel$defineInteropFlag(a) {
  Object.defineProperty(a, '__esModule', {value: true, configurable: true});
}


var $69723b528af5e761$exports = {};



var $448bf2346fc667f4$exports = {};

$parcel$export($448bf2346fc667f4$exports, "Btn", () => $448bf2346fc667f4$export$37f54d0b7407611f);



const $448bf2346fc667f4$export$37f54d0b7407611f = ({ children: children , onClick: onClick  })=>{
    return /*#__PURE__*/ (0, $8zHUo$reactjsxruntime.jsx)("button", {
        className: "Btn",
        onClick: onClick,
        children: children
    });
};


var $3a0520e5e12da36d$exports = {};

$parcel$export($3a0520e5e12da36d$exports, "Icon", () => $3a0520e5e12da36d$export$f04a61298a47a40f);



var $3230dc5a9470a8e6$exports = {};

$parcel$defineInteropFlag($3230dc5a9470a8e6$exports);

$parcel$export($3230dc5a9470a8e6$exports, "default", () => $3230dc5a9470a8e6$export$2e2bcd8739ae039);
var $73283c691903a7c5$exports = {};
$73283c691903a7c5$exports = new URL("help.cfb7a18f.png", "file:" + __filename).toString();


var $970c31b781d2e3e3$exports = {};
$970c31b781d2e3e3$exports = new URL("shelter.952f7ece.png", "file:" + __filename).toString();


const $3230dc5a9470a8e6$var$IconsList = {
    help: (/*@__PURE__*/$parcel$interopDefault($73283c691903a7c5$exports)),
    shelter: (/*@__PURE__*/$parcel$interopDefault($970c31b781d2e3e3$exports))
};
var $3230dc5a9470a8e6$export$2e2bcd8739ae039 = (icon)=>{
    return $3230dc5a9470a8e6$var$IconsList[icon];
};


const $3a0520e5e12da36d$export$f04a61298a47a40f = ({ i: i , alt: alt  })=>{
    return /*#__PURE__*/ (0, $8zHUo$reactjsxruntime.jsx)("img", {
        className: "Icon",
        src: (0, $3230dc5a9470a8e6$export$2e2bcd8739ae039)(i) + "",
        alt: alt || ""
    });
};



$parcel$exportWildcard($69723b528af5e761$exports, $448bf2346fc667f4$exports);
$parcel$exportWildcard($69723b528af5e761$exports, $3a0520e5e12da36d$exports);
$parcel$exportWildcard($69723b528af5e761$exports, $3230dc5a9470a8e6$exports);


$parcel$exportWildcard(module.exports, $69723b528af5e761$exports);


//# sourceMappingURL=main.js.map
