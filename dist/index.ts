import help from "./help.png";
import shelter from "./shelter.png";

const IconsList = {
    help,
    shelter
}

export type Icons = 'help' | 'shelter';

export default (icon: Icons) => {
    return IconsList[icon]
}