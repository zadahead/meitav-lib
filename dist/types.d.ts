import * as React from "react";
export const Btn: React.FC<IBtn>;
export interface IBtn {
    onClick: (e: any) => void;
    children: React.ReactNode;
}
export type Icons = 'help' | 'shelter';
export const Icon: ({ i, alt }: IIcon) => JSX.Element;
export interface IIcon {
    i: Icons;
    alt?: string;
}

//# sourceMappingURL=types.d.ts.map
