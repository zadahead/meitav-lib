import './index.css';
import './app.css';
import 'assets/fonts/fonts.css';

export * from './Elements/Btn/Btn';
export * from './Elements/Icon/Icon';
export * from './Elements/Icon/icon-getter';