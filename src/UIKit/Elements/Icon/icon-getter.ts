import help from "assets/icons/help.png";
import shelter from "assets/icons/shelter.png";

const IconsList = {
    help,
    shelter
}

export type Icons = 'help' | 'shelter';

export default (icon: Icons) => {
    return IconsList[icon]
}