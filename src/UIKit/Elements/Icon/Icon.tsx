import * as React from 'react';

import './Icon.scss';
import $, { Icons } from './icon-getter';

export const Icon = ({ i, alt }: IIcon) => {
    return (
        <img className='Icon' src={$(i) + ''} alt={alt || ''} />
    )
}


export interface IIcon {
    i: Icons,
    alt?: string
}