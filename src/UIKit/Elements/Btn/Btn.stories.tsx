// Button.stories.ts|tsx

import * as React from 'react';

import { ComponentStory, ComponentMeta } from '@storybook/react';

import { Btn, Icon } from 'UIKit';

export default {
    /* 👇 The title prop is optional.
    * See https://storybook.js.org/docs/react/configure/overview#configure-story-loading
    * to learn how to generate automatic titles
    */
    title: 'Btn',
    component: Btn,
} as ComponentMeta<typeof Btn>;

const handleClick = () => {
    console.log('click');
}

export const Primary: ComponentStory<typeof Btn> = () => (
    <div>
        <Btn onClick={handleClick}>Button </Btn>
        <Icon i={"help"} />
        <Icon i={'shelter'} />
    </div>
);