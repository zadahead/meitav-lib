import * as React from 'react';
import './Btn.scss';
export const Btn: React.FC<IBtn> = ({ children, onClick }) => {
    return <button className='Btn' onClick={onClick}>{children}</button>
}

export interface IBtn {
    onClick: (e: any) => void,
    children: React.ReactNode
}